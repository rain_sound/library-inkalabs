module.exports = (sequelize, type) => {
    return sequelize.define('books', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        title: {
            type: type.STRING(100),
        },
        date_of_pub: {
            type: type.DATE,
        },
        edition: {
            type: type.STRING(80),
        },
        book_img: {
            type: type.STRING(255),
        },
        quantity: {
            type: type.INTEGER
        },
        state: {
            type: type.BOOLEAN
        }
       
    })
}