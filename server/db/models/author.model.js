module.exports = (sequelize, type) => {
    return sequelize.define('authors', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: type.STRING(255),
        },
        address: {
            type: type.STRING(255),
        },
        cellphone: {
            type: type.STRING(9),
        }
    })
}