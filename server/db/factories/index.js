const faker = require("faker");
const bcrypt = require("bcryptjs")
const { User, Book, Author, BookAuthor } = require("../../db");


for(var i = 0; i < 15; i++){
    var pass = 'ss';
    var name = faker.name.firstName();
    User.create({
        name: name,
        email: faker.internet.email(),
        imgProfile: 'default.png',
        password: bcrypt.hashSync(pass, 8)
    });

    Author.create({
        name:  faker.name.findName(),
        address:  faker.address.streetAddress(),
        cellphone:  faker.phone.phoneNumber('054######')
    }).then(author => {
        Book.create({
            title: faker.random.word(),
            date_of_pub:  faker.date.past(),
            edition:  faker.random.word(),
            book_img:  'https://picsum.photos/id/440/640/480',
            quantity:  faker.random.number(),
            state:  faker.random.boolean(),
            authorId: 1
        })
    })
    

}

