var router = require('express').Router()
    //Definir rutas
const users = require('./users')
const books = require('./books')
const authors = require('./authors')


//Controladores
router.use('/users', users)

router.use('/authors', authors)

router.use('/books', books)

module.exports = router