const { upload, sendJson } = require("../../utilities");
const { Author, Book, BookAuthor } = require("../../db");

const getAll = (req, res, next) => {
  Author.findAll({
      attributes: ['id','name']
  }).then(authors => sendJson(res, "", false, false, authors));
};

const getById = (req, res, next) => {
    Book.findAll({
      attributes: ['title'],
      include: [
        {
          model: Author,
          required: true,
          attributes: ['id','name'],
          where: {
            id: req.params.id
          }
        }
      ]
    }).then(books => sendJson(res, "", false, false, books));
};


module.exports = {
  getAll,
  getById,
};
