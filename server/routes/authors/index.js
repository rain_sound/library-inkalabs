const router = require("express").Router();

const authorService = require("./author.service");
const { verifyToken } = require("../middleware");


router.get("/",  authorService.getAll);
router.get("/:id",  authorService.getById);


module.exports = router;
