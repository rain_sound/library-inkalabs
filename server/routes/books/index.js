const router = require("express").Router();

const bookService = require("./book.service");
const { verifyToken } = require("../middleware");


router.get("/",  bookService.getAll);
router.get("/:id",  bookService.getById);
router.get("/getbooks/:id",  bookService.getBooksFilter);

router.post("/update",bookService.updateBook);
router.post("/new", bookService.newBook);
router.post("/delete", bookService.delBook);
module.exports = router;
