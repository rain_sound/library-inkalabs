const {
  upload,
  sendJson
} = require("../../utilities");
const {
  conn,
  Book,
  Author,
  BookAuthor
} = require("../../db");

const getAll = (req, res, next) => {
  //Recupera los libros habilitados
  Book.findAll({
    where: {
      state: true
    },
    attributes: [
      "id",
      "title",
      "date_of_pub",
      "edition",
      "book_img",
      "quantity"
    ],
    include: [{
      attributes: ["name"],
      model: Author,
      required: true
    }]
  }).then(books => sendJson(res, "", false, false, books));
};

const getBooksFilter = (req, res, next) => {

  const filter = req.params.id;
  Book.findAll({
      attributes: [
        "id",
        "title",
        "date_of_pub",
        "edition",
        "book_img",
        "quantity"
      ],
      where: {
        title: {
          $like: '%' + filter + '%'
        }
      }
    }).then(books => sendJson(res, '', false, false, books))
    .catch(err => sendJson(res, 'Error al recuperar la lista de libros', true, true, err))
};


const getById = (req, res, next) => {
  Book.findOne({});
};

const newBook = (req, res, next) => {
  const authorId = req.body.authorId;

  Book.create({
      title: req.body.title,
      date_of_pub: req.body.date_of_pub,
      edition: req.body.edition,
      book_img: req.body.book_img,
      quantity: req.body.quantity,
      state: true,
      authorId: req.body.authorId
    })
    .then(book =>
      sendJson(res, "Se creó con éxito el libro", false, false, book)
    )
    .catch(err => {
      sendJson(res, "No se pudo crear el libro", true, true, err);
    });
};

const delBook = (req, res, next) => {
  const book = req.body.id;
  Book.findOne({
    where: {
      id: book
    }
  }).then(book => {
    if (!book) {
      return sendJson(res, "No se encontró el libro", true, true, {});
    }
    book
      .update({
        state: false
      })
      .then(dat => sendJson(res, "Se eliminó con éxito", false, false, {}))
      .catch(err => {
        sendJson(res, "Hubo un error al eliminar", true, true, err);
      });
  });
};

const updateBook = (req, res, next) => {
  const book = req.body.book;
  Book.findOne({
    where: {
      id: book
    }
  }).then(book => {
    if (!book) {
      return sendJson(res, "No se encontró el libro", true, true, err);
    }
    book
      .update({
        title: req.body.title,
        date_of_pub: req.body.date_of_pub,
        edition: req.body.edition,
        book_img: req.body.book_img,
        quantity: req.body.quantity
      })
      .then(book => sendJson(res, "Se actualizó con éxito", false, false, {}))
      .catch(err =>
        sendJson(res, "Error al actualizar el libro", true, true, err)
      );
  });
};

module.exports = {
  getAll,
  getById,
  newBook,
  delBook,
  updateBook,
  getBooksFilter
};