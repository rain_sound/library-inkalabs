const multer = require("multer");
const router = require("express").Router();

const userService = require("./user.service");
const { verifyToken } = require("../middleware");
const { upload } = require("../../utilities");


router.get("/resetpassword", userService.resetPassword);
router.post("/forgotpassword", userService.forgotPassword);
router.put("/updatepassword", userService.updatePassword);

router.get("/", userService.getAll);
router.get("/:id", verifyToken, userService.getById);
router.post("/signin", userService.signin);
router.post("/signup", userService.signup);

router.post("/uploadImage", userService.uploadImage);

module.exports = router;
