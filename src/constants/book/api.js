import { URL } from '../../helpers/api'

const GET_ALL             = `${URL}books/` 
const NEW_BOOK            = `${URL}books/new` 
const DELETE_BOOK         =  `${URL}books/delete/` 


export const bookConstants = {
    GET_ALL,
    NEW_BOOK,
    DELETE_BOOK
}