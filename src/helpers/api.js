import axios from 'axios';
import { store } from '../store/index'

export const configFormData = { headers: { "Content-Type": "multipart/form-data" } };

export const URL = 'http://localhost:3001/'
export const URL_PUBLIC = 'http://localhost:3001/public/'


const authHeader = () => {
  let user = JSON.parse(localStorage.getItem('user'))
  if(user && user.accessToken) {
      return {
          'access-token': user.accessToken
      }
  }
}


const client = axios.create({
  baseURL: URL,
  headers: authHeader(),
  responseType: 'json', 
})

export default client;