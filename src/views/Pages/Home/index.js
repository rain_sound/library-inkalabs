import React, { Component } from 'react';
import ListCardBook from '../../../components/CardBook'

class Home extends Component {

    constructor(){ super(); }

    render() {
        return(
            <div className="container mt-5">
                <ListCardBook/>
            </div>
        )
    }
}


export default Home