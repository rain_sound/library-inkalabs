import api from "../helpers/api";
import { bookConstants } from "../constants/book/api";


const getAll = () => {
  return api
    .get(bookConstants.GET_ALL)
    .then(res => res)
    .catch(err => err);
};

const newBook = () => {

}

const deleteBook = id => {
   return api
   .post(bookConstants.DELETE_BOOK, {
     id
   }).then(res => res)
   .catch(err => err);
}

export const bookService = {
  getAll,
  newBook,
  deleteBook
};
