import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { connect } from "react-redux";

import Home from "./views/Pages/Home";


class App extends Component {
  render() {
    return (
      <div className="container">
        <Router>
          <React.Fragment>
              <Route exact path="/" component={Home} />
              <Route exact path="/authors" component={Home} />
          </React.Fragment>
        </Router>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  user: state.user && state.user
});

export default connect(
  mapStateToProps,
  null
)(App);
