import { combineReducers } from 'redux'
import { user, getUserId as _getUserId, getUsername as _getUsename } from './user'
import { userConstants } from '../constants/user/actions'

 const appReducer = combineReducers({
    user,
})

export const rootReducer = (state, action) => {
    if (action.type === userConstants.LOGOUT) {
        state = undefined
      }
    
      return appReducer(state, action)
  }
