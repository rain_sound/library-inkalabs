import React, { Component } from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import { bookService } from '../../services/bookService'

const CardBook = ({ handleUpdate, handleDelete, id, img_url, title, date, author}) => {
    return (
        <div className="col-8 col-md-3 ">
          <div className="card">
            <img
              src={img_url}
              className="cardbook img-fluid card-img-top"
              alt="..."
            />
            <div className="card-body">
              <h5 className="card-title text-muted">{title}</h5>
              <span className="cardbook-subtitle">{ author }</span>
              
              <p className="card-text cardbook-content">
               Publicado el: {date.substr(0,10)}
              </p>
              <div className="row">
                <div className="col-6">
                  <button onClick={e => handleUpdate(title, id, img_url,date,author)} href="#" className="btn btn-primary">
                    Actualizar
                  </button>
                </div>
                <div className="col-6">
                  <button onClick={e => handleDelete(id)} href="#" className="btn btn-danger">
                    Eliminar
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
    );
}



class ListCardBook extends Component {
    constructor(){
        super();
        this.state = {
            books: [],
            filterBooks: [],
            modalUpdate: false,
            modalDelete: false,
            modalNew: false,
            title: '',
            author: '',
            date: '',
            edition: '',
            quantity: 0,
            message: '',
            textFilter: ''
        }
    }

    handleModalCreate = () => {
      this.setState(prev => ({ modalNew: !prev.modalNew }))
    }

    handleModalUpdate = (title, id, img_url,date,author) => {
      console.log('mdal update', title)
    }

    handleModalDelete = id => {
      bookService.deleteBook(id).then(res => {
        const { filterBooks } = this.state
        var newList = filterBooks.filter(item => item.id !== id)
        this.setState(() => ({  filterBooks: newList, books: newList }))
        alert(res.data.message)
      })
    }

    handleChange = e => {
      const { value } = e.target
      this.setState(() => ({ textFilter: value }))
      const { books, textFilter, filterBooks } = this.state
      if(textFilter === ''){
        this.setState(() => ({ filterBooks: books }))
      }
      else {
        var tmpFilter = books.filter(item => {
          let title = item.title.toLowerCase();
          let author = item.author.name.toLowerCase();
          return author.includes(textFilter.toLowerCase()) || title.includes(textFilter.toLowerCase());
        });
        this.setState(() => ({ filterBooks: tmpFilter }))
      }
      
    }

    componentDidMount(){
        bookService.getAll().then(res => this.setState({ books: res.data.data, filterBooks: res.data.data }));
    }

    render() {
        const { filterBooks, modalNew } = this.state
        return (
          <React.Fragment>
            <input
              className="form-control"
              onChange={this.handleChange}
              type="text"
              placeholder="Ingresa un libro"
            />
            <button
              onClick={() => this.handleModalCreate()}
              className="mt-2 mb-2 btn btn-primary"
            >
              +Añadir
            </button>
            <div className="row">
              {filterBooks.map(item => (
                <CardBook
                  id={item.id}
                  handleUpdate={this.handleModalUpdate}
                  handleDelete={this.handleModalDelete}
                  key={item.id}
                  title={item.title}
                  author={item.author.name}
                  img_url={item.book_img}
                  date={item.date_of_pub}
                />
              ))}
            </div>

            {/* MODAL CREATE */}
            {modalNew && (
              <div>
                <Modal isOpen={modalNew} toggle={this.handleModalCreate}>
                  <ModalBody>Desea Eliminar el registro?</ModalBody>
                  <ModalFooter>
                    <Button color="danger" onClick={this.handleDelete}>
                      Eliminar
                    </Button>{" "}
                    <Button color="primary" onClick={this.handleModalCreate}>
                      Cancel
                    </Button>
                  </ModalFooter>
                </Modal>
              </div>
            )}
            {/* END MODAL CREATE */}

            {/* MODAL DELETE */}
            {modalNew && (
              <div>
                <Modal isOpen={modalNew} toggle={this.handleModalDelete}>
                  <ModalBody>Desea Eliminar el registro?</ModalBody>
                  <ModalFooter>
                    <Button color="danger" onClick={this.handleDelete}>
                      Eliminar
                    </Button>{" "}
                    <Button color="primary" onClick={this.handleModalDelete}>
                      Cancel
                    </Button>
                  </ModalFooter>
                </Modal>
              </div>
            )}
            {/* END MODAL DELETE */}
          </React.Fragment>
        );
    }
}
export default ListCardBook;